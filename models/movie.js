var mongoose = require('../common/db');
var Movie = new mongoose.Schema({
    movieName: String,
    movieImg: String,
    movieVideo: String,
    movieDownload: String,
    movieTime: String,
    movieNumSuppose: Number,
    movieNumDownload: Number,
    movieMainPage: Boolean
});

var movieModel = mongoose.model('movie', Movie);

module.exports = movieModel;