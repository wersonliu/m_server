var mongoose = require('../common/db');
var Comment = new mongoose.Schema({
    movie_id: String,
    username: String,
    context: String,

    check: Boolean
});

Comment.statics.findByMovieId = function (m_id, callBack) {
    this.find({movie_id: m_id, check: true}, callBack)
};

Comment.statics.findAll = function (callBack) {
    this.find({}, callBack)

};

var commentModel = mongoose.model('comment', Comment);

module.exports = commentModel;